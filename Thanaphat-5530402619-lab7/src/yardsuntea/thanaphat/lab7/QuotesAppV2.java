package yardsuntea.thanaphat.lab7;

import java.awt.*;

import javax.swing.*;

public class QuotesAppV2 extends QuotesAppV1 {
	
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */

	private static final long serialVersionUID = 6659778562556060664L;
	

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run(){
				QuotesAppV2 qa = new QuotesAppV2("Quotes App V2");
				qa.addComponents();
				qa.addMenus();
			}
		});
	}

	protected void addComponents() {
		
		JPanel panelCurrent = new JPanel(); 		// Panel �Ѻ��ͤ��� "Current Quote"
		JLabel current = new JLabel("Current Quote:");
		panelCurrent.add(current);
		
		JTextArea textArea = new JTextArea(10,15);	// TextArea �Ѻ��ͤ������
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setText("\n �������Ҿ�ǧ�� ʹյ��ҹ����� ���Ш����ҧ �ѹ���ҹ����� �Ѻ��ͧ����� ͹Ҥ������֧�ѡ��");
		textArea.append("�֧��������������繻Ѩ�غѹ ��觷���Դ��͹��������� ��觷��Դ��Ҩ��Դ��� ��ǹ�����Ҿ�ǧ��");
		textArea.append("\n ������Ƿ���դ������� ������Ƿ���繨�ԧ ��ͻѨ�غѹ�͹����ͧ \n");
		textArea.append("\n �������ͷء��觷�����Թ �ҡ���Թ����ͧ���ҧ����ͧ�ͧ����� ���Թ��仡Ѻ�� ���ҡ���Թ�ҧ����ͧ�������");
		textArea.append("\n �ͧ����� ����º������ �ѧࡵ��ó� ���¹��������ԧ���µ���ͧ ���ٴ�֧�����蹷�����������˹�� �������͹���");
		textArea.append("\n �ǡ�ҡ��ѧ��觿ѧ�������ͧ���ǡѺ������� ���������·�ȹ� ����þԨ�ó�����ͨС���Ƕ֧�ؤ�ŷ����� \n");
		textArea.append("\n -Achara Klinsuwan");
		textArea.setFont(new Font("resif" , Font.ITALIC ,16)); // ��˹� Font ��� ��Ҵ��ͤ���
		textArea.setForeground(Color.BLUE);
		
		JScrollPane sp = new JScrollPane(textArea);	// ScorllPane ��� �� ScorllBar ��������͹��ͤ���������

		JPanel panelLanguage = new JPanel();		// Panel Language 
		JPanel subPanelLanguage = new JPanel();
		panelLanguage.setLayout(new GridLayout(1,0));
		JLabel language = new JLabel("Language:");
		JRadioButton thai = new JRadioButton("Thai" , true);
		JRadioButton english = new JRadioButton("Engligh");
		subPanelLanguage.add(thai);			
		subPanelLanguage.add(english);
		panelLanguage.add(language);
		panelLanguage.add(subPanelLanguage);
		
		JPanel panelTypes = new JPanel();		// Panel Types
		JPanel subPanelTypes = new JPanel();
		panelTypes.setLayout(new GridLayout(1,0));
		JLabel types = new JLabel("Types:");
		JCheckBox wisdom = new JCheckBox("Wisdom", true);
		JCheckBox technology = new JCheckBox("Technology");
		JCheckBox socity = new JCheckBox("Socity");
		JCheckBox politics = new JCheckBox("Politics");
		subPanelTypes.add(wisdom);
		subPanelTypes.add(technology);
		subPanelTypes.add(socity);
		subPanelTypes.add(politics);
		panelTypes.add(types);
		panelTypes.add(subPanelTypes);
		
		JPanel panelNumber = new JPanel();		// Panel Number
		panelNumber.setLayout(new GridLayout(1,0));
		JLabel number = new JLabel("Number of quotes:");
		JComboBox numberList = new JComboBox();
		numberList.addItem("1");
		numberList.addItem("2");
		numberList.addItem("3");
		panelNumber.add(number);
		panelNumber.add(numberList);
		
		JPanel panelAuthors = new JPanel();		// Panel Authors
		panelAuthors.setLayout(new GridLayout(1,0));
		JLabel authors = new JLabel("Authors");
		String authorsList[] = {"Achara Klinsuwan", "Buddha", "Dungtrin", "Earl Nightingale", 
								"Einstein", "Ghandi", "Phra Paisal Visalo" , "V.Vajiramedhi" };
								// �Ѻ��͵�ҧ������ Arrays 
		JList listAuthors = new JList(authorsList);
		JScrollPane scorllAuthors = new JScrollPane(listAuthors);
		authors.add(scorllAuthors);
		panelAuthors.add(authors);
		panelAuthors.add(listAuthors);
		
		JPanel panelButton = new JPanel(); 		// Panel Button
		JButton submit = new JButton("Submit");
		JButton cancel = new JButton("Cancel");
		panelButton.add(submit);
		panelButton.add(cancel);
		

		JPanel subPanel1 = new JPanel();	// ���ҧ Panel ������ͨѴ�ٻẺ �ͧ Language,Types,Number
		subPanel1.setLayout(new GridLayout(3,0));
		subPanel1.add(panelLanguage);
		subPanel1.add(panelTypes);
		subPanel1.add(panelNumber);
				
		JPanel panelNorth = new JPanel(); 	// ���ҧ Panel ������ͨѴ�ٻẺ�ͧ Current ���Դ����
		panelNorth.setLayout(new BorderLayout());	
		panelNorth.add(panelCurrent,BorderLayout.WEST);
		
		JPanel panelSouth = new JPanel();	// ���ҧ Panel ������ͨѴ�ٻẺ�ͧ subPanel1,
		panelSouth.setLayout(new BorderLayout());	// Authors,Button ������§�ҡ��ŧ��ҧ
		panelSouth.add(subPanel1,BorderLayout.NORTH);
		panelSouth.add(panelAuthors,BorderLayout.CENTER);
		panelSouth.add(panelButton,BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();		// Panel ������Ѵ�ٻẺ �ͧ Panel North,sp,South
		panel.setLayout(new BorderLayout());
		panel.add(panelNorth,BorderLayout.NORTH);
		panel.add(sp,BorderLayout.CENTER);
		panel.add(panelSouth,BorderLayout.SOUTH);
		
		window.add(panel);
		window.pack();
		
		}
		
	public QuotesAppV2(String string) {
		
		window = new JFrame(string);
		window.setLocation(250,70);
		window.setSize(500,650);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
	}
		
}



