package yardsuntea.thanaphat.lab7;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class QuotesAppV1 extends JPanel {
	
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */

	private static final long serialVersionUID = 6929872432948146444L;
	protected JFrame window;						// ��С�ȵ������ Protected ������ҹ������ Method
	protected JMenuItem mNew,mOpen,mSave,mExit;
	protected JMenuItem mRed,mGreen,mBlue;
	protected JMenuItem m16,m18,m20;
	protected JMenuItem mBold,mItalic,mBI;
	BufferedImage img;
	
	
	public void paint(Graphics g){
		g.drawImage(img,0,0,null);
	}
	
	public QuotesAppV1(){
		try{
			img = ImageIO.read(new File("bin/image/time.jpeg"));
			}
		catch (IOException e){
			e.printStackTrace(System.err);
			}
		}
	
	/*public Dimension getPreferredSize(){
		if(img == null){
			return new Dimension(100,100);
		}else{
			return new Dimension(img.getWidth(),img.getHeight());
		}
	}*/
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				QuotesAppV1 qa = new QuotesAppV1("Quotes App V1");
				qa.addMenus();
			}
		});	
	}

	protected void addMenus() {

		JMenuBar menuBar = new JMenuBar(); 		// Menus 

		JMenu menuFile = new JMenu("File");		// ������ѡ File ��� Edit
		JMenu menuEdit = new JMenu("Edit");
		menuBar.add(menuFile);
		menuBar.add(menuEdit);

		mNew = new JMenuItem("New");		// �������¢ͧ File
		menuFile.add(mNew);
		mOpen = new JMenuItem("Open", 
							new ImageIcon("bin/image/file-open-icon.png")); // Icon
		menuFile.add(mOpen);
		mSave = new JMenuItem("Save");
		menuFile.add(mSave);
		mExit = new JMenuItem("Exit");
		menuFile.add(mExit);

		JMenu submenuColor = new JMenu("Set color");  // Set color ���������¢ͧ Edit
		mRed = new JMenuItem("Red");  	// �������¢ͧ Set Color
		submenuColor.add(mRed);
		mGreen = new JMenuItem("Green");
		submenuColor.add(mGreen);
		mBlue = new JMenuItem("Blue");
		submenuColor.add(mBlue);
		menuEdit.add(submenuColor);

		JMenu menuFont = new JMenu("Set Font"); // Set Font ���������¢ͧ Edit
		menuEdit.add(menuFont);

		JMenu submenuSize = new JMenu("Size"); // Size ���������¢ͧ Edit
		m16 = new JMenuItem("16"); // �������¢ͧ Size
		submenuSize.add(m16);
		m18 = new JMenuItem("18");
		submenuSize.add(m18);
		m20 = new JMenuItem("20");
		submenuSize.add(m20);
		menuFont.add(submenuSize);

		JMenu submenuStyle = new JMenu("Style"); // Style ���������¢ͧ Style
		mBold = new JMenuItem("Bold"); // �������¢ͧ Style
		submenuStyle.add(mBold);
		mItalic = new JMenuItem("Italic");
		submenuStyle.add(mItalic);
		mBI= new JMenuItem("Bold Italic");
		submenuStyle.add(mBI);
		menuFont.add(submenuStyle);

		window.setJMenuBar(menuBar);
	}
	
	public QuotesAppV1(String string) {
		
		
		window = new JFrame(string);
		window.setLocation(100,100);
		window.setSize(500,650);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		QuotesAppV1 content = new QuotesAppV1();
		window.setContentPane(content);
		
	}

}
